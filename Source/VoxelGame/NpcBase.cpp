// Fill out your copyright notice in the Description page of Project Settings.


#include "NpcBase.h"

// Sets default values
ANpcBase::ANpcBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANpcBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANpcBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ANpcBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

